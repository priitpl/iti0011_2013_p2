package ee.pold.iti0011.prax2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileCache {
	
//	private 
	public static boolean fileExists(String filename){
		File f = new File(filename);
		if(f.exists()) {
			return true;
		}
		return false;
	}
	
	public static String convertToFilename(String data){
		data = data.toLowerCase();
//		CharSequence a = "äõöüабвгдеёжзийклмнопрстэю";
//		CharSequence b = "aoouabvgdeezziiklmnoprsteu";
		String a = "äõöüабвгдеёжзийклмнопрстэю";
		String b = "aoouabvgdeezziiklmnoprsteu";
		
		for(int i=0; i < a.length(); i++){
			data = data.replace(a.charAt(i), b.charAt(i));
		}
		data = data.replaceAll("[\\W]", "_").replaceAll("[_]{2,}", "_");
		return data;
		
	}
	
	/**
	 * Read method.
	 * Reads given filename content and return it as String.
	 * @param filename
	 * @return
	 */
	public static String read(String filename) throws Exception{
		
		String data = "";
		try {
			File file = new File(filename);
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while((line = br.readLine( )) != null) {  
				data += line;
			}
			br.close();
			
		} catch(Exception e){
			Main.debug("ERROR on file reading: " + e.toString());
			throw e;
		}
		return data;
	}
	
	/**
	 * Writes given content to given file.
	 * @param filename
	 * @param text
	 */
	public static void write(String filename, String text) {
		try {
			Writer output = null;
			File file = new File(filename);
			output = new BufferedWriter(new FileWriter(file));
			output.write(text);
			output.close();
		} catch(IOException ioE){
			Main.debug("ERROR: FileCache: " + ioE.toString());
		} catch(Exception e){
			
		}
		
	}
	
}
