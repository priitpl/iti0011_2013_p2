package ee.pold.iti0011.prax2;

abstract public class AbstractResponse {
	
//	private String status;
	
	private String data;
	
	private boolean empty = true;
	
	abstract protected void parse(String data);
	
	/**
	 * Remove intersections with given object
	 * @param response
	 */
	abstract public void removeIntersections(AbstractResponse response);
	
	/**
	 * Main constructor
	 */
	public AbstractResponse(){}
	
	/**
	 * Main constructor - set data.
	 * @param data
	 */
	public AbstractResponse(String data){
		setData(data);
	}
	
	/**
	 * Set data. Usually, this is response from service - Text
	 * @param data
	 */
	public void setData(String data){
		this.empty = false;
		this.data = data;
		this.parse(data);
	}
	
	/**
	 * Get data
	 * @return
	 */
	public String getData(){
		return data;
	}
	
	/**
	 * Check, if response is empty or not.
	 * @return
	 */
	public boolean isEmpty(){
		return empty;
	}
	
	public void clear() {
		empty = true;
		data = null;
	}
}
