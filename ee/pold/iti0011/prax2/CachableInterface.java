package ee.pold.iti0011.prax2;

public interface CachableInterface {
	public String getFilename();
	public String getCacheFilenamePart();
}
