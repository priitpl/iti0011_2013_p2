package ee.pold.iti0011.prax2.exceptions;

public class AttributeValueIsNotCorrectException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5138240496886757026L;

	public AttributeValueIsNotCorrectException(String key, String value){
		super("Value for attribute '" + key + "' is wrong or empty: '" + value + "'");
	}
}
