package ee.pold.iti0011.prax2.exceptions;

public class AttributeValueIsMissingException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1877985936155101290L;

	public AttributeValueIsMissingException(String key){
		super("Missing value for option: " + key);
	} 
}
