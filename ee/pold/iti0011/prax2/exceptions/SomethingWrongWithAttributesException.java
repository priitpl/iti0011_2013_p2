package ee.pold.iti0011.prax2.exceptions;

public class SomethingWrongWithAttributesException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 145738010240770382L;

	public SomethingWrongWithAttributesException(String[] args){
		super("Something wrong with your attributes: " + args.toString());
	}
}
