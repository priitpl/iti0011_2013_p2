package ee.pold.iti0011.prax2.exceptions;

public class AttributeOptionValueIsWrongException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5350490629119718362L;

	public AttributeOptionValueIsWrongException(String key, String option){
		super("Attribute '" + key + "' doesn't have such option '" + option + "'");
	}
}
