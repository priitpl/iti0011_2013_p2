package ee.pold.iti0011.prax2.exceptions;

public class ProblemsWithInternetConnectionException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 228895344969929495L;

	public ProblemsWithInternetConnectionException(){
		super("Problems with internet connect. And it's seems we don't have cache also.");
	}
}
