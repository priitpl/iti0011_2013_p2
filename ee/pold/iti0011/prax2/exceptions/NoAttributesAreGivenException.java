package ee.pold.iti0011.prax2.exceptions;

public class NoAttributesAreGivenException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7844546948135476335L;

	
	public NoAttributesAreGivenException(){
		super("No parameters entered.\nYou have to define at least location name.");
	}
}
