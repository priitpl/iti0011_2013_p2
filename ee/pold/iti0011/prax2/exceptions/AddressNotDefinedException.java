package ee.pold.iti0011.prax2.exceptions;

public class AddressNotDefinedException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8698211527780075774L;

	public AddressNotDefinedException() {
		super("Address does not defined. Try again.");
	}
}
