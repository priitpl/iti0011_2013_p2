package ee.pold.iti0011.prax2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;

import ee.pold.iti0011.prax2.exceptions.ProblemsWithInternetConnectionException;

public abstract class AbstractRequest {
	
	/**
	 * Flag shows if we forced to download data from service
	 */
	private boolean forceDownload = false;
	
	/**
	 * Flag - show only those items, that not appears in cache
	 */
	private boolean onlyLatest = false;
	
	private int timeout = 1000;
	
	/**
	 * Retrieve request URL
	 * @return
	 */
	abstract protected String getRequestUrl();
	
	/**
	 * Retrieve filename for cache
	 * @return
	 */
	abstract protected String getCacheFilenamePart();
	
	/**
	 * Creates response object
	 * @return
	 */
	abstract protected AbstractResponse createResponse();
	
	/**
	 * Creates response object for given request.
	 * @param request
	 * @return
	 */
	abstract protected AbstractResponse createResponse(AbstractRequest request);
	
	/**
	 * Get request timeout
	 * @return
	 */
	public int getTimeout(){
		return timeout;
	}
	
	/**
	 * Set request timeout
	 * @param milliseconds
	 */
	public void setTimeout(int milliseconds){
		timeout = milliseconds;
	}
	
	/**
	 * Set to force downloading
	 * @param force
	 */
	public void forceDownload(boolean force){
		if (!force && onlyLatest) return;
		forceDownload = force;
	}
	
	/**
	 * Get only latest works.
	 * @param onlyLatest
	 */
	public void onlyLatest(boolean onlyLatest){
		this.onlyLatest = onlyLatest;
		if (onlyLatest) {
			forceDownload(true);
		}
	}
	
	/**
	 * Get properly formated cache filename
	 * @return
	 */
	public String getCacheFilename(){
		return FileCache.convertToFilename(getCacheFilenamePart()) + ".txt";
	}
	
	/**
	 * Sends GET Request to the url.
	 * URL is generated using getRequestUrl().
	 * It also caches response from the server.
	 * @return Response
	 */
	public AbstractResponse sendRequest() throws ProblemsWithInternetConnectionException{
		AbstractResponse downloadedResponse = createResponse();
		AbstractResponse cacheResponse = createResponse();
		
		String cacheFilename = getCacheFilename();
		
		Main.debug("Check, if cache exists: " + cacheFilename);
		
		// If Cache file exists, read it.
		if (FileCache.fileExists(cacheFilename)) {
			Main.debug("Cache exists, loading: " + cacheFilename);
			try{
				cacheResponse.setData(FileCache.read(cacheFilename));
			} catch(Exception e){
				Main.debug("Problems with reading from cache: " + cacheFilename);
				Main.debug("Will try to download.");
				cacheResponse.clear();
			}
			
			// If we haven't been forced to download, return response.
			if (!forceDownload && !cacheResponse.isEmpty()) return cacheResponse;
		}
		
		// If set to forceDownload, then do it.
		if (forceDownload || cacheResponse.isEmpty()) {
			Main.debug("Force to download");
			try {
				downloadedResponse = submitRequest();
			} catch(Exception e){
				Main.debug("Problems with retrieving data from internet.");
				if (!cacheResponse.isEmpty()) {
					return cacheResponse;
				}
				
				throw new ProblemsWithInternetConnectionException();
			}
			
			try{
				FileCache.write(getCacheFilename(), downloadedResponse.getData());
			} catch(Exception e){
				Main.debug(e.getMessage());
				Main.debug("Ops... Error on wrting cache: " + cacheFilename);
				Main.debug("It's ok... we can live without cache ... nothing special...");
				Main.debug("At least we have internet connection. Do we?");
			}
			
			if (onlyLatest) {
				Main.debug("Removing intersection");
				downloadedResponse.removeIntersections(cacheResponse);
			}
		}
		
		return downloadedResponse;
	}
	
	/**
	 * Make request and download data from service
	 * @return Response
	 */
	protected AbstractResponse submitRequest() throws SocketTimeoutException{
		AbstractResponse response = createResponse(this);
		try{
			String responseData = "";
			URL url = new URL(getRequestUrl());
			URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
			url = uri.toURL();
			String urlStr = url.toString();
			
			Main.debug("Send request to: " + urlStr);
			
	        URLConnection yc = url.openConnection();
	        yc.setConnectTimeout(getTimeout());
	        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
	        String inputLine;

	        while ((inputLine = in.readLine()) != null) {
	        	responseData += inputLine;
	        }
	        in.close();
	        response.setData(responseData);
			
		}
		catch (SocketTimeoutException e){
			throw e;
		}
		catch(Exception e){
//			 if error appears - no connection or something,
//			 and if implements CachableInterface
//			 the use cache.
			System.out.println("ERROR I NREQUEST" + e.toString());
		}
		
		return response;
	}
}
