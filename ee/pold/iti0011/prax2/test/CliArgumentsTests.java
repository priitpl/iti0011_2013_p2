package ee.pold.iti0011.prax2.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ee.pold.iti0011.prax2.CliArguments;
import ee.pold.iti0011.prax2.exceptions.AddressNotDefinedException;
import ee.pold.iti0011.prax2.exceptions.AttributeOptionValueIsWrongException;
import ee.pold.iti0011.prax2.exceptions.NoAttributesAreGivenException;

public class CliArgumentsTests {

	private CliArguments cliArgs;
    @Before
    public void setUp() {
    	String[] args = {};
    	cliArgs = new CliArguments(args);
    	
    	cliArgs.addKeyValuesListArgument("sort", "title,date", "Sort images by one of the field");
		cliArgs.addKeyValueArgument("t", "Set connection timeout in milliseconds");
		cliArgs.addSingleValueArgument("f", "Force to download");
		cliArgs.addSingleValueArgument("l", "Force to download and show only latest");
		cliArgs.addSingleValueArgument("d", "Print debug info");
    }

    @After
    public void tearDown() {
    }
    
	@Test(expected = NoAttributesAreGivenException.class)
	public void test_empty_attributes_list() throws Exception{
		String[] args = {};
		cliArgs.setArgs(args);
		cliArgs.validate();
//		fail("Not yet implemented");
	}
	
	@Test(expected = AddressNotDefinedException.class)
	public void test_attributes_without_address(){
		String[] args = {"-f", "-sort", "title"};
		cliArgs.setArgs(args);
		cliArgs.validate();
	}

	@Test(expected = AttributeOptionValueIsWrongException.class)
	public void test_wrong_attribute_option(){
		String[] args = {"-f", "-sort", "NOT_EXISTED_ONE", "Tallinn"};
		cliArgs.setArgs(args);
		cliArgs.validate();
	}
	
	@Test()
	public void test_glag_is_set(){
		String[] args = {"-f", "Tallinn"};
		cliArgs.setArgs(args);
		cliArgs.validate();
		assertEquals(true, cliArgs.isOn("f"));
	}
	
	@Test()
	public void test_address_retrived_properly(){
		String address = "Tallinn";
		String[] args = {"-f", "-f", "-f", "-f",  address};
		cliArgs.setArgs(args);
		cliArgs.validate();
		assertEquals(address, cliArgs.getAddress());
	}
}
