package ee.pold.iti0011.prax2.panoramio;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;
import org.json.JSONString;

import ee.pold.iti0011.prax2.Main;

public class Image implements Serializable, JSONString{
	
	private static final long serialVersionUID = 1L;

	/**
	 * Image Height
	 */
	private int height;
	
	/**
	 * Image width
	 */
	private int width;
	
	/**
	 * Latitude
	 */
	private String lat;
	
	/**
	 * Longitude
	 */
	private String lon;
	
	/**
	 * Owner ID
	 */
	private int ownerId;
	
	/**
	 * Owner name
	 */
	private String ownerName;
	
	/**
	 * Owner url
	 */
	private String ownerUrl;
	
	/**
	 * Photo URL
	 */
	private String photoFileUrl;
	
	/**
	 * Photo ID
	 */
	private int photoId;
	
	/**
	 * Photo title
	 */
	private String photoTitle;
	
	/**
	 * Photo URL
	 */
	private String photoUrl;
	
	/**
	 * Place ID
	 */
	private String placeId;
	
	/**
	 * Uploaded date
	 */
	private Date uploadedDate;

	@Override
	public String toJSONString() {
		return "HERE";
	}
	
	/**
	 * Convert object to string.
	 */
	public String toString()
	{
		SimpleDateFormat df = new SimpleDateFormat("Y-MM-dd");
		return 
				df.format(getUploadedDate()) + " "
				+ "\"" + getPhotoTitle() + "\"" + " " + "\"" + getOwnerName() + "\"" + " " + getPhotoUrl();
	}
	
	
	@Override
	public boolean equals(Object v){
		boolean retVal = false;

		if (v instanceof Image){
			Image ptr = (Image) v;
			retVal = ptr.getPhotoId() == this.getPhotoId();
		}
		
		return retVal;
	}
	
	@Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.getPhotoId();
        return hash;
    }
	
	/**
	 * Main constructor.
	 * Create new Image using given JSONObject.
	 * @param json
	 */
	public Image(JSONObject json) {
		try{
			setHeight(json.getInt("height"));
			setWidth(json.getInt("width"));
			setLat(String.valueOf(json.getDouble("latitude")));
			setLon(String.valueOf(json.getDouble("longitude")));
			setOwnerId(json.getInt("owner_id"));
			setOwnerName(json.getString("owner_name"));
			setOwnerUrl(json.getString("owner_url"));
			setPhotoFileUrl(json.getString("photo_file_url"));
			setPhotoId(json.getInt("photo_id"));
			setPhotoTitle(json.getString("photo_title"));
			setPhotoUrl(json.getString("photo_url"));
			Date dd = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH).parse(json.getString("upload_date"));
			setUploadedDate(dd);
		} catch(Exception e){
			Main.debug(e.toString());
		}
	}
	
	/**
	 * Set height
	 * @return
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Set height
	 * @param height
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Get Width
	 * @return
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Set Width
	 * @param width
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Get latitude
	 * @return
	 */
	public String getLat() {
		return lat;
	}

	/**
	 * Set latitude
	 * @param lat
	 */
	public void setLat(String lat) {
		this.lat = lat;
	}

	/**
	 * Get longitude
	 * @return
	 */
	public String getLon() {
		return lon;
	}

	/**
	 * Set longitude
	 * @param lon
	 */
	public void setLon(String lon) {
		this.lon = lon;
	}

	/**
	 * Get owner ID
	 * @return
	 */
	public int getOwnerId() {
		return ownerId;
	}

	/**
	 * Set owner ID
	 * @param ownerId
	 */
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * Get Owner name
	 * @return
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * set Owner name
	 * @param ownerName
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * Get owner URL
	 * @return
	 */
	public String getOwnerUrl() {
		return ownerUrl;
	}

	/**
	 * Set Owner URL
	 * @param ownerUrl
	 */
	public void setOwnerUrl(String ownerUrl) {
		this.ownerUrl = ownerUrl;
	}

	/**
	 * Get Photo file url
	 * @return
	 */
	public String getPhotoFileUrl() {
		return photoFileUrl;
	}

	/**
	 * Set photo file url
	 * @param photoFileUrl
	 */
	public void setPhotoFileUrl(String photoFileUrl) {
		this.photoFileUrl = photoFileUrl;
	}

	/**
	 * Get photo ID
	 * @return
	 */
	public int getPhotoId() {
		return photoId;
	}

	/**
	 * Set photo ID
	 * @param photoId
	 */
	public void setPhotoId(int photoId) {
		this.photoId = photoId;
	}

	/**
	 * Get Photo Title
	 * @return
	 */
	public String getPhotoTitle() {
		return photoTitle;
	}

	/**
	 * Set photo title
	 * @param photoTitle
	 */
	public void setPhotoTitle(String photoTitle) {
		this.photoTitle = photoTitle;
	}

	/**
	 * Get photo url
	 * @return
	 */
	public String getPhotoUrl() {
		return photoUrl;
	}

	/**
	 * set photo url
	 * @param photoUrl
	 */
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	/**
	 * get place Id
	 * @return
	 */
	public String getPlaceId() {
		return placeId;
	}

	/**
	 * set place id
	 * @param placeId
	 */
	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}
	
	
	/**
	 * Get uploaded date
	 * @return
	 */
	public Date getUploadedDate() {
		return uploadedDate;
	}

	/**
	 * set uploaded date
	 * @param uploadedDate
	 */
	public void setUploadedDate(Date uploadedDate) {
		this.uploadedDate = uploadedDate;
	}	
}
