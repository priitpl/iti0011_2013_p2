package ee.pold.iti0011.prax2.panoramio;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;

import ee.pold.iti0011.prax2.Main;
import ee.pold.iti0011.prax2.AbstractResponse;

public class PanoramioResponse extends AbstractResponse{

	private int imagesTotal;
	
	private ArrayList<Image> images = new ArrayList<Image>();
	
	public ArrayList<Image> getImages(){
		return images;
	}
	
	public int getImagesCount(){
		return imagesTotal;
	}
	
	@Override
	protected void parse(String data) {
		try{
			JSONObject json = new JSONObject(Main.parseUnicodeJsonString(data));
			
			JSONArray photos = json.getJSONArray("photos");
			for(int i=0; i < photos.length(); i++){
				JSONObject photo = photos.getJSONObject(i);
				Image img = new Image(photo);
				images.add(img);
			}
			imagesTotal = Integer.valueOf(json.get("count").toString());
		} catch(Exception e){
			System.out.println("ERROR: PanoramioResponse.parse: " + e.toString());
		}
		
	}
	
	@Override
	public void removeIntersections(AbstractResponse compareWith){
		Main.debug("Now we will try to remove interactions");
		
		PanoramioResponse response = (PanoramioResponse)compareWith;
		
		if (response.getImages().size() <= 0) return;
		
		Iterator<Image> responsIterator = response.getImages().iterator();
		
		while(responsIterator.hasNext()){
			Image image = responsIterator.next();
			if(getImages().contains(image)){
				this.images.remove(image);
				continue;
			}
		}
	}
	
	public class CustomComparator implements Comparator<Image> {

		@Override
		public int compare(Image o1, Image o2) {
			// TODO Auto-generated method stub
			return 0;
		}
	}

}
