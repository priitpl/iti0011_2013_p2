package ee.pold.iti0011.prax2.panoramio;

import ee.pold.iti0011.prax2.AbstractRequest;
import ee.pold.iti0011.prax2.AbstractResponse;
import ee.pold.iti0011.prax2.googlemaps.Bounds;

public class PanoramioRequest extends AbstractRequest //implements CachableInterface
{
	/**
	 * Searchable bounds object.
	 */
	private Bounds bounds;

	/**
	 * Search given bounds
	 * @param bounds
	 */
	public void searchBounds(Bounds bounds){
		this.bounds = bounds;
	}
	
	@Override
	protected String getCacheFilenamePart(){
		return "pano_" + bounds.getName();
	}
	
	@Override
	protected String getRequestUrl() {
		String url  = "http://www.panoramio.com/map/get_panoramas.php?set=public&" 
				+ "from=0&to=20&minx=%s&miny=%s&maxx=%s&maxy=%s&size=medium&mapfilter=true";
		return String.format(url, 
				bounds.getSouthwest().getY(), bounds.getSouthwest().getX(),
				bounds.getNortheasy().getY(), bounds.getNortheasy().getX()
		);
	}
	
	@Override
	protected AbstractResponse createResponse() {
		return new PanoramioResponse();
	}
	
	@Override
	protected AbstractResponse createResponse(AbstractRequest request) {
		return createResponse();
	}
	

}
