package ee.pold.iti0011.prax2;

import java.util.HashMap;
import java.util.Map;

import ee.pold.iti0011.prax2.exceptions.AddressNotDefinedException;
import ee.pold.iti0011.prax2.exceptions.AttributeOptionValueIsWrongException;
import ee.pold.iti0011.prax2.exceptions.AttributeValueIsMissingException;
import ee.pold.iti0011.prax2.exceptions.AttributeValueIsNotCorrectException;
import ee.pold.iti0011.prax2.exceptions.NoAttributesAreGivenException;
import ee.pold.iti0011.prax2.exceptions.SomethingWrongWithAttributesException;

public class CliArguments 
{
	private String address;
	
	/**
	 * List of registered Key=Value attributes.
	 */
	private HashMap<String, String> keyValue = new HashMap<String, String>();
	
	/**
	 * Storage for user entered attributes 
	 */
	private HashMap<String, String> values_keyValue = new HashMap<String, String>();
	
	/**
	 * List of registered Key=Value(list of options) attributes
	 */
	private HashMap<String, String[]> keyValueList = new HashMap<String, String[]>();
	
	/**
	 * List of registered single KEY(simply ON flag) attributes
	 */
	private HashMap<String, String> singleValue = new HashMap<String, String>();
	
	/**
	 * Storage for user entered ON flags
	 */
	private HashMap<String, Boolean> values_singleValue = new HashMap<String, Boolean>();
	
	
	/**
	 * User CLI arguments
	 */
	private String[] cliArgs;
	
	
	public CliArguments(String[] args) //throws Exception
	{
		setArgs(args);
	}
	
	/**
	 * Set args.
	 * @param args
	 */
	public void setArgs(String[] args){
		cliArgs = args;
		address = null;
		
		values_keyValue = new HashMap<String, String>();
		values_singleValue = new HashMap<String, Boolean>();
	}
	
	/**
	 * Retrieve location name entered by user
	 * @return
	 */
	public String getAddress(){
		return address;
	}

	/**
	 * Check, if ON Flag attribute with given key is set
	 * @param key
	 * @return
	 */
	public boolean isOn(String key){
		return (values_singleValue.containsKey(key));
	}
	
	public String get(String key){
		if (values_keyValue.containsKey(key)) {
			return values_keyValue.get(key);
		}
		return null;
	}
	
	/**
	 * Get value by key. If is not set, return default
	 * @param key
	 * @return
	 */
	public String get(String key, String defaultValue){
		if (values_keyValue.containsKey(key)) {
			return values_keyValue.get(key);
		}
		return defaultValue;
	}
	
	/**
	 * Validate CL arguments entered by user
	 */
	public void validate() {
		int argsLength = cliArgs.length;
		
		if (argsLength == 0) {
			throw new NoAttributesAreGivenException();
		}
		
//		Main.debug("ARGS Length: " + argsLength);
		// if only 1 lement is set and it's a key, then throw an exception
 		if (argsLength == 1 && isKeyArgument(cliArgs[0])) {
			throw new AddressNotDefinedException();
		}
//		// if the last argument is a key, then throw exception
//		if (isKeyArgument(cliArgs[argsLength - 1])) {
//			throw new AddressNotDefinedException();
//		}
		
		for(int i = 0; i < argsLength; i++){
//			Main.debug("ELEMENT: " + cliArgs[i]);
			if (isKeyArgument(cliArgs[i])) {
				String key = cliArgs[i].replaceAll("-", "");
				
				if (singleValue.containsKey(key)) {
//					Main.debug("Option ON: " + key);
					if (values_singleValue.containsKey(key)) {
						values_singleValue.remove(key);
					}
					
					values_singleValue.put(key, true);
					continue;
				}
				
				if (keyValue.containsKey(key)) {
//					Main.debug("Option  KEY=VALUE: " + key);
					if (i + 2 >= argsLength) {
						throw new AddressNotDefinedException();
					}
					if (isKeyArgument(cliArgs[i+1])) {
						throw new AttributeValueIsMissingException(key);
					}
					
					if (values_keyValue.containsKey(key)) {
						values_keyValue.remove(key);
					}
					
					values_keyValue.put(key, cliArgs[++i]);
					continue;
					
				}
				if (keyValueList.containsKey(key)) {
//					Main.debug("Option  KEY=OPTION: " + key);
					String[] properties = keyValueList.get(key);
					String[] _options = properties[0].split(",");
					boolean _found = false;
					for(int o = 0 ; o < _options.length; o++ ){
//						Main.debug("Options: " + _options[o] + "=" + cliArgs[i+1]);
						if (_options[o].equals(cliArgs[i + 1])) {
							registerValueForKeyValue(key, cliArgs[++i]);
							_found = true;
							break;
						}
					}
					if (_found) {
						continue;
					}
					throw new AttributeOptionValueIsWrongException(key, cliArgs[i+1]);
				}
			} else if(i+1 == argsLength) {
				address = cliArgs[i];
			} else {
				throw new SomethingWrongWithAttributesException(cliArgs);
			}
		}
		if (address == null || !isValueCorrect(address)) {
			throw new AddressNotDefinedException();
		}
	}
	
	/**
	 * Helper, helps to identify if value is given correctly.
	 * Empty string is not valid
	 * @param value
	 * @return
	 */
	protected boolean isValueCorrect(String value){
		if (value.length() == 0) return false;
		return true;
	}
	
	/**
	 * Register CL argument value in our registry
	 * @param key
	 * @param value
	 */
	protected void registerValueForKeyValue(String key, String value) {
		if (!isValueCorrect(value)) {
			throw new AttributeValueIsNotCorrectException(key, value);
		}
		if (values_keyValue.containsKey(key)) {
			values_keyValue.remove(key);
		}
		values_keyValue.put(key, value);
	}
	
	/**
	 * Check, if given string is KEY argument
	 * @param check
	 * @return
	 */
	protected boolean isKeyArgument(String check) {
		if (check.length() == 0) return false;
		
//		Main.debug("CHECK IF IS KEY: " + check);
//		Main.debug("\tIs key?: " + (check.trim().charAt(0) == '-'));
		return check.trim().charAt(0) == '-';
	}

	/**
	 * Register new Key Value(options list) argument.
	 * 
	 * @param key - argument name
	 * @param options - representation of the list, each option separated by ,
	 * @param comment - attribute comment
	 * @return
	 */
	public CliArguments addKeyValuesListArgument(String key, String options, String comment){
		String[] data = {options, comment};
		keyValueList.put(key, data);
		return this;
	}
	
	/**
	 * Register new CL Key Value argument
	 * @param key - CL argument name
	 * @param comment - comment
	 * @return
	 */
	public CliArguments addKeyValueArgument(String key, String comment){
		keyValue.put(key, comment);
		return this;
	}
	
	/**
	 * Register single ON Flag argument. 
	 * @param key
	 * @param comment
	 * @return
	 */
	public CliArguments addSingleValueArgument(String key, String comment) {
		singleValue.put(key, comment);
		return this;
	}
	
	/**
	 * Will generate usage info about each registered CL argument
	 * @return
	 */
	public String usageInfo(){
		String s = "";
		s += "\nUsage: java -jar 139095_prax2.jar [OPTIONS] location";
		s += "\n\n\nAvailable OPTIONS:";
		
		for (Map.Entry<String, String[]> entry : keyValueList.entrySet()) {
			String[] data = entry.getValue();
			s += "\n\t-" + entry.getKey() + " <" + data[0] + ">" + "\t - " + data[1];
		}
		
		for (Map.Entry<String, String> entry : keyValue.entrySet()) {
			s += "\n\t-" +entry.getKey() + " <VALUE>" + "\t - " + entry.getValue();
		}
		
		for (Map.Entry<String, String> entry : singleValue.entrySet()) {
			s += "\n\t-" +entry.getKey() + "" + "\t - " + entry.getValue();
		}
		
		
		return s;
	}
}
