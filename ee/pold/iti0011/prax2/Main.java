package ee.pold.iti0011.prax2;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ee.pold.iti0011.prax2.googlemaps.Bounds;
import ee.pold.iti0011.prax2.googlemaps.GoogleMapsRequest;
import ee.pold.iti0011.prax2.googlemaps.GoogleMapsXmlResponse;
import ee.pold.iti0011.prax2.panoramio.Image;
import ee.pold.iti0011.prax2.panoramio.PanoramioRequest;
import ee.pold.iti0011.prax2.panoramio.PanoramioResponse;

public class Main {

	/**
	 * Flag - output debug info or not
	 */
	public static boolean debug = false;
	
	/**
	 * Flag - output info messages or not
	 */
	public static boolean inform = true;
	
	/**
	 * Sort by - user defined options. If empty, will sort by title.
	 */
	public static String sorting;
	
	/**
	 * User defined sorting method: one of 'asc' or 'desc'
	 */
	public static String sortingMethod;
	
	/**
	 * Default timeout value.
	 */
	public static final String TIMEOUT = "5000";
	
	public static void mainTest(String[] args){
//		String[] argsa = {"-l", "-sort", "title",  "Kohtla jarve"};
//		String[] argsa = {"-l", "-d", "Kohtla Jarve"};
//		String[] argsa = {"-sort", "date", "-d", "Kohtla Jarve"};
//		String[] argsa = {"-t", "100", "-sort", "date", "-f", "Kohtla Jarve"};
		String[] argsa = {"-t", "100", "-s", "title", "-st", "asc",  "Kohtla Jarve"};
//		cliArgs.setArgs(argsa);
	}
	
	public static void main(String[] args) {
		
		CliArguments cliArgs = new CliArguments(args);
		
		// Here we register CLI incoming arguments
		// For simple ON value use addSingleValueArgument
		// For key value use addKeyValueArgument.
		// When registered, you can simply retrive by.
		cliArgs.addKeyValuesListArgument("s", "title,date,author", "Sort images by one of the field");
		cliArgs.addKeyValuesListArgument("st", "asc,desc", "Sorting method: ASCending or DESCending");
		cliArgs.addKeyValueArgument("t", "Set connection timeout in milliseconds");
		cliArgs.addSingleValueArgument("f", "Force to download");
		cliArgs.addSingleValueArgument("l", "Force to download and show only latest");
		cliArgs.addSingleValueArgument("d", "Print debug info");
		String[] argsa = {"Kohtla Jarve"};
		cliArgs.setArgs(argsa);
//		cliArgs.setArgs(args);
		
		try{
			cliArgs.validate();
		} catch(Exception e){
			System.out.println(e.getMessage());
			System.out.println(cliArgs.usageInfo());
			System.exit(0);
		}

		Main.debug("Flags: force: " + cliArgs.isOn("f") + ", latest: " + cliArgs.isOn("l"));
		
		// User can turn ON debugging. in this case some info will be shown.
		debug = cliArgs.isOn("d");

		// Create Google Maps request
		GoogleMapsRequest gmapRequest = new GoogleMapsRequest();
		
		// Create Panoramio Request
		PanoramioRequest apiPanoramio = new PanoramioRequest();
		apiPanoramio.forceDownload(cliArgs.isOn("f"));
		apiPanoramio.onlyLatest(cliArgs.isOn("l"));
		apiPanoramio.setTimeout(Integer.parseInt(cliArgs.get("t", TIMEOUT)));
		
		// Set searchable address
		gmapRequest.searchFor(cliArgs.getAddress());
		
		// Set force flag - 
		gmapRequest.forceDownload(cliArgs.isOn("f"));
		gmapRequest.onlyLatest(cliArgs.isOn("l"));
		gmapRequest.setTimeout(Integer.parseInt(cliArgs.get("t", TIMEOUT)));
		
		try {
			GoogleMapsXmlResponse response = new GoogleMapsXmlResponse();
			try { 
				response = (GoogleMapsXmlResponse)gmapRequest.sendRequest();
			} catch(Exception ee){
				throw ee;
//				System.out.println("Main: " + ee.getMessage());
			}
			
			Iterator<Bounds> boundsIterator = response.getBounds().iterator();
			if (response.getBounds().size() == 0) {
				Main.inform("Google Maps API couldn't identify your address: '" + gmapRequest.getUserLocationName() + "'");
			}
			while(boundsIterator.hasNext()){
				Bounds current = boundsIterator.next();
				apiPanoramio.searchBounds(current);
				PanoramioResponse panoResponse = (PanoramioResponse)apiPanoramio.sendRequest();
				
				// Check, if images exists.
				if (panoResponse.getImages().size() == 0) {
					if (cliArgs.isOn("l")) {
						Main.inform("There are not new images for given address: '" + current.getName() + "'");
					} else {
						Main.inform("There are not images for given address: '" + current.getName() + "'");
					}
					continue;
				}
				
				// Sory by and sorting method
				sorting = cliArgs.get("s", "title");
				sortingMethod = cliArgs.get("st", "asc");
				
				if (sorting != null) {
					Collections.sort(panoResponse.getImages(), new Comparator<Image>() {
				        @Override public int compare(Image p1, Image p2) {
				        	Image c1;
				        	Image c2;
				        	// Here we do reverse ordering - change places for variables.
				        	switch(sortingMethod) {
					        	case "desc":
					        		c1 = p2;
					        		c2 = p1;
					        		break;
					        	case "asc":
				        		default:
				        			c1 = p1;
				        			c2 = p2;
				        			break;
				        	}
				        	switch (sorting){
				        		case "author":
				        			return c1.getOwnerName().toLowerCase().compareTo(c2.getOwnerName().toLowerCase());
				        		case "date":
				        			return c1.getUploadedDate().compareTo(c2.getUploadedDate());
					        	case "title":
				        		default:
				        			return c1.getPhotoTitle().toLowerCase().compareTo(c2.getPhotoTitle().toLowerCase());
				        	}
				        }

				    });
				}
			    
				Iterator<Image> images = panoResponse.getImages().iterator();
				
				int counter = 0;
				// Here we run through all images and output it
				while(images.hasNext()){
					Image image = images.next();
					System.out.println(padTo(++counter) + ": " + image);
				}
			}
		} catch(Exception e){
			System.out.println(e.getMessage());
			System.out.println("Maybe you should try once more ?");
		}
	}
	
	/**
	 * Simple method for filling string with spaces.
	 * @param i
	 * @return
	 */
	public static String padTo(int i){
		if (i < 10) {
			return " " + i;
		}
		return "" + i;
	}

	/**
	 * Debug method - simple outputs messages depending on debug mode.
	 * If debug mode is ON, will output all messages from the system, otherwise no.
	 * @param message
	 */
	public static void debug(String message){
		if(debug) System.out.println(message);
	}
	
	public static void inform(String info){
		if(inform) System.out.println(info);
	}
	
	/**
	 * A quick and dirty function to parse json encoded unicode
	 * string into Java String.<p>
	 * 
	 * The function finds all multi-byte symbols in the form \\uXXXX 
	 * (with one \). The part after u is treated as hexadecimal number
	 * representing the code of one symbol.
	 *  
	 * @param jsonString Encoded json string
	 * @return Java String
	 */
	public static String parseUnicodeJsonString(String jsonString) {
		String s = "";
		int i;
		for (i = 0; i < jsonString.length() - 5; i++) {
			String part = jsonString.substring(i, i + 6);
			Matcher m = Pattern.compile("\\\\u([0-9a-zA-Z]{4})").matcher(part);
			if (m.matches()) {
				s += Character.toString((char) Integer.parseInt(m.group(1), 16));
				i += 5;
				continue;
			}
			s += jsonString.charAt(i);
		}
		while (i < jsonString.length()) {
			s += jsonString.charAt(i++);
		}
		return s;
	}
}
