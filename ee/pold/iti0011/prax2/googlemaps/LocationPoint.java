package ee.pold.iti0011.prax2.googlemaps;

public class LocationPoint {
	
	/**
	 * X coordinate
	 */
	private String x;
	
	/**
	 * Y coordinate
	 */
	private String y;
	
	/**
	 * Constructor
	 */
	public LocationPoint(){
		
	}
	
	/**
	 * Constructor. Create object with given coordinates.
	 */
	public LocationPoint(String x, String y){
		this.x = x;
		this.y = y;
	}

	/**
	 * Comparison method.
	 * Compare location X and Y coordinates
	 * 
	 * @param compareWith
	 * @return
	 */
	public boolean equals(LocationPoint compareWith){
		if (!getX().equals(compareWith.getX())) return false;
		if (!getY().equals(compareWith.getY())) return false;
		return true;
	}
	
	public void setX(String x){
		this.x = x;
	}
	
	/**
	 * Get X coordinate
	 * @return
	 */
	public String getX() {
		return x;
	}

	/**
	 * Set Y coordinate
	 * @param y
	 */
	public void setY(String y){
		this.y = y;
	}
	
	/**
	 * Get Y coordinate
	 * @return
	 */
	public String getY() {
		return y;
	}
	
	
	
	
}
