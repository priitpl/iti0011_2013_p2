package ee.pold.iti0011.prax2.googlemaps;

import ee.pold.iti0011.prax2.CachableInterface;
import ee.pold.iti0011.prax2.FileCache;
import ee.pold.iti0011.prax2.AbstractRequest;
import ee.pold.iti0011.prax2.AbstractResponse;

public class GoogleMapsRequest extends AbstractRequest implements CachableInterface{

	private String locationName;
	
	/**
	 * Set location name
	 * @param name
	 */
	public void searchFor(String name){
		locationName = name;
	}
	
	/**
	 * Get location name
	 * @return
	 */
	public String getUserLocationName(){
		return locationName;
	}
	
	@Override
	public String getCacheFilenamePart(){
		return FileCache.convertToFilename("gmpa_" + locationName);
	}
	
	@Override
	protected String getRequestUrl(){
		String url = "http://maps.googleapis.com/maps/api/geocode/xml?address=%s&sensor=false";
		return String.format(url, locationName);
	}
	
	@Override
	public String getFilename() {
		return locationName;
	}
	
	@Override
	protected AbstractResponse createResponse() {
		return new GoogleMapsXmlResponse();
	}
	
	@Override
	protected AbstractResponse createResponse(AbstractRequest request) {
		return new GoogleMapsXmlResponse(request);
	}
}
