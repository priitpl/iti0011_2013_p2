package ee.pold.iti0011.prax2.googlemaps;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import ee.pold.iti0011.prax2.AbstractRequest;
import ee.pold.iti0011.prax2.AbstractResponse;

public class GoogleMapsXmlResponse extends AbstractResponse{
	
	private String status;
	
	private String name;
	
	private ArrayList<Bounds> bounds = new ArrayList<Bounds>();
	
	private Bounds current;
	
	private AbstractRequest request;
	
	/**
	 * Main constructor
	 */
	public GoogleMapsXmlResponse(){
		super();
	}
	
	public GoogleMapsXmlResponse(AbstractRequest request){
		super();
		this.request = request;
	}
	
	@Override
	public void removeIntersections(AbstractResponse compareWith) {
		Iterator<Bounds> boundsIterator = bounds.iterator();
		GoogleMapsXmlResponse response = (GoogleMapsXmlResponse)compareWith;
		
		if (response.getBounds().size() <= 0) return;
		
		while(boundsIterator.hasNext()){
			Bounds _bounds = boundsIterator.next();
			if(response.getBounds().contains(_bounds)){
				this.bounds.remove(_bounds);
				continue;
			}
		}
	}
	
	@Override
	protected void parse(String data){
		try { 
			GoogleMapsXmlHandler gmapHandler = new GoogleMapsXmlHandler(this);
			
			// Create InputStrem from text for parsing
			InputStream stream = new ByteArrayInputStream(data.getBytes("UTF-8"));
			
			XMLReader xr = XMLReaderFactory.createXMLReader();
			
			xr.setContentHandler(gmapHandler);
			xr.parse( new InputSource(stream));
		} catch (Exception e) {
			System.out.println("ERROR: GoogleMapsXmlResponse: " + e.toString());
			System.out.println("DATA: \n" + data + "\n-----------------");
			
		}
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		if (request != null && request instanceof GoogleMapsRequest) {
			GoogleMapsRequest _request = (GoogleMapsRequest)request;
			return _request.getUserLocationName();
		}
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Bounds> getBounds() {
		return bounds;
	}

	public void addBounds(Bounds boundsO){
		current = boundsO;
		bounds.add(boundsO);
//		current = bounds.get(bounds.size() - 1);
	}
	
	public void setBounds(ArrayList<Bounds> bounds) {
		this.bounds = bounds;
	}
	
	public Bounds getCurrent(){
		return current;
	}

	
	
}
