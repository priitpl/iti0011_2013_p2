package ee.pold.iti0011.prax2.googlemaps;

import java.io.Serializable;


public class Bounds implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Address name/string
	 */
	private String name;
	
	/**
	 * Bounds southwest coordinate
	 */
	private LocationPoint southwest;
	
	/**
	 * Bounds northeast coordinate
	 */
	private LocationPoint northeasy;

	public Bounds(){
		southwest = new LocationPoint();
		northeasy = new LocationPoint();
	}
	
	public boolean equals(Bounds compareWith){
		if (!southwest.equals(compareWith.getSouthwest())) return false;
		if (!northeasy.equals(compareWith.getNortheasy())) return false;
		return true;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocationPoint getSouthwest() {
		return southwest;
	}

	public void setSouthwest(LocationPoint southwest) {
		this.southwest = southwest;
	}

	public LocationPoint getNortheasy() {
		return northeasy;
	}

	public void setNortheasy(LocationPoint northeasy) {
		this.northeasy = northeasy;
	}
	
	
}
