package ee.pold.iti0011.prax2.googlemaps;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

/**
 * XML parser/handler.
 * Converts Google Maps response representation into Bounds object.
 * @author priitpl
 *
 */
public class GoogleMapsXmlHandler extends DefaultHandler {
   // Override methods of the DefaultHandler class
   // to gain notification of SAX Events.
   //
        // See org.xml.sax.ContentHandler for all available events.
   //
	
	private GoogleMapsXmlResponse response;
	
	private String lastElementText = "";
	
	private String mpath = "";
	
	private boolean debug = false;
	
	public GoogleMapsXmlHandler(GoogleMapsXmlResponse response){
		this.response = response;
	}
	/**
	 * Get parsed response.
	 * @return
	 */
	public GoogleMapsXmlResponse getResponse()
	{
		return response;
	}
	
	@Override
	public void startElement(String namespaceURI, String localName, String qName,
			Attributes attr ) throws SAXException 
	{
		if (debug) {
			System.out.println( "SAX Event: START ELEMENT[ " + localName + " ]" );
			for ( int i = 0; i < attr.getLength(); i++ ){
				System.out.println( "   ATTRIBUTE: " + attr.getLocalName(i) + " VALUE: " + attr.getValue(i));
			}
		}
		
		if (mpath.length() > 1) mpath += ".";
		mpath += localName;
		
		switch(mpath){
			case "GeocodeResponse.result":
				response.addBounds(new Bounds());
				break;
//			case "GeocodeResponse.result.geometry.bounds":
////				response.addBounds(new Bounds());
//				break;
		}
			
	}
	
	@Override
	public void endElement(String namespaceURI, String localName, String qName ) throws SAXException 
	{
		
		if (mpath == localName) {
			mpath = "";
		} else {
			int pos = mpath.lastIndexOf("." + localName);
		    if (pos > -1) {
		    	mpath = mpath.substring(0, pos);
		    }
		}
		
		if (debug) System.out.println( "SAX Event: END ELEMENT[ " + localName + " ]" );
	}
	
	@Override
	public void characters( char[] ch, int start, int length ) throws SAXException 
	{
		lastElementText = "";		
		if (debug) System.out.println(mpath);
		
		try {
			lastElementText = String.valueOf(ch, start, length);
			
			switch(mpath){
				case "GeocodeResponse.status":
					response.setStatus(lastElementText);
					break;
					
				case "GeocodeResponse.result.address_component.short_name":
					
					break;
					
				case "GeocodeResponse.result.address_component.long_name":
					break;
					
				case "GeocodeResponse.result.formatted_address":
					if (!lastElementText.isEmpty() && response.getCurrent() != null){
						response.getCurrent().setName(lastElementText);
					}
					break;
					
//				case "GeocodeResponse.result.geometry.bounds":
//					response.addBounds(new Bounds());
//					break;
				case "GeocodeResponse.result.geometry.bounds.southwest.lat":
					response.getCurrent().getSouthwest().setX(lastElementText);
					break;
					
				case "GeocodeResponse.result.geometry.bounds.southwest.lng":
					response.getCurrent().getSouthwest().setY(lastElementText);
					break;
					
				case "GeocodeResponse.result.geometry.bounds.northeast.lat":
					response.getCurrent().getNortheasy().setX(lastElementText);
					break;
					
				case "GeocodeResponse.result.geometry.bounds.northeast.lng":
					response.getCurrent().getNortheasy().setY(lastElementText);
					break;
			}
		} catch (Exception e) {
			System.out.println("ERROR : GoogleMapsXmlHandler: characters" + e.toString());
			e.printStackTrace();
		}
		if (debug) System.out.println(lastElementText);
	}
}
